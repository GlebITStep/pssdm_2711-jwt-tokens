﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkApi.ViewModels
{
    public class RegisterCredentials
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [Compare("PasswordAgain")]
        public string Password { get; set; }

        [Required]
        public string PasswordAgain { get; set; }

        [Required]
        public string FullName { get; set; }

        public string PhotoUrl { get; set; }

        public DateTime? DateOfBirth { get; set; }
    }
}
