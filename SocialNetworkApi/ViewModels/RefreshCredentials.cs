﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkApi.ViewModels
{
    public class RefreshCredentials
    {
        [Required]
        public string RefreshToken { get; set; }
    }
}
