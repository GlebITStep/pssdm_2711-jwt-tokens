﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SocialNetworkApi.Identity;
using SocialNetworkApi.Models;
using SocialNetworkApi.Services;
using SocialNetworkApi.ViewModels;

namespace SocialNetworkApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class AccountsController : ControllerBase
    {
        private readonly UserManager<AppUser> userManager;
        private readonly SocialNetworkResourcesDbContext resourcesDbContext;
        private readonly SocialNetworkIdentityDbContext identityDbContext;
        private readonly TokenGenerator tokenGenerator;

        public AccountsController(
            UserManager<AppUser> userManager,
            SocialNetworkResourcesDbContext resourcesDbContext,
            SocialNetworkIdentityDbContext identityDbContext,
            TokenGenerator tokenGenerator)
        {
            this.userManager = userManager;
            this.resourcesDbContext = resourcesDbContext;
            this.identityDbContext = identityDbContext;
            this.tokenGenerator = tokenGenerator;
        }

        /// <summary>
        /// Register a new user
        /// </summary>
        /// <param name="register">Register information</param>
        /// <returns>Created user profile</returns>
        [HttpPost("register")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<Profile>> Register(RegisterCredentials register)
        {
            if (ModelState.IsValid)
            {
                var user = new AppUser { Email = register.Email, UserName = register.Email };
                var result = await userManager.CreateAsync(user, register.Password);
                if (result.Succeeded)
                {
                    var profile = new Profile
                    {
                        FullName = register.FullName,
                        DateOfBirth = register.DateOfBirth,
                        PhotoUrl = register.PhotoUrl,
                        AppUserId = user.Id
                    };
                    await resourcesDbContext.Profiles.AddAsync(profile);
                    await resourcesDbContext.SaveChangesAsync();
                    return profile;
                }
            }
            return BadRequest();
        }

        /// <summary>
        /// Login user
        /// </summary>
        /// <param name="login">Login data</param>
        /// <returns>Refresh and access tokens</returns>
        [HttpPost("login")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        public async Task<ActionResult<UserTokens>> Login(LoginCredentials login)
        {
            if (ModelState.IsValid)
            {
                var user = await userManager.FindByNameAsync(login.Email);

                if (user != null)
                {
                    bool correctPassword = await userManager.CheckPasswordAsync(user, login.Password);
                    if (correctPassword)
                    {
                        var refreshToken = new RefreshToken
                        {
                            Token = tokenGenerator.GenerateRefreshToken(),
                            AppUser = user,
                            Expires = DateTime.Now + TimeSpan.FromDays(30)
                        };
                        await identityDbContext.RefreshTokens.AddAsync(refreshToken);
                        await identityDbContext.SaveChangesAsync();

                        var userTokens = new UserTokens
                        {
                            AccessToken = tokenGenerator.GenereateAccessToken(user),
                            RefreshToken = refreshToken.Token
                        };

                        return userTokens;
                    }
                }
            }
            return Unauthorized();
        }

        [Authorize]
        [HttpGet("test")]
        public ActionResult<string> Test()
        {
            return "Secured string!";
        }

        /// <summary>
        /// Genereate new pair of refresh and access tokens
        /// </summary>
        /// <param name="refresh">Refresh token</param>
        /// <returns>Refresh and access token</returns>
        [HttpPost("refresh")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        public async Task<ActionResult<UserTokens>> Refresh(RefreshCredentials refresh)
        {
            var oldToken = identityDbContext.RefreshTokens
                .Include(x => x.AppUser)
                .FirstOrDefault(x => x.Token == refresh.RefreshToken);

            if (oldToken == null)
            {
                return Unauthorized();
            }
            else if (oldToken != null && oldToken.Expires < DateTime.Now)
            {
                identityDbContext.RefreshTokens.Remove(oldToken);
                await identityDbContext.SaveChangesAsync();
                return Unauthorized();
            }
            else
            {
                var newToken = new RefreshToken
                {
                    Token = tokenGenerator.GenerateRefreshToken(),
                    AppUserId = oldToken.AppUserId,
                    Expires = DateTime.Now + TimeSpan.FromDays(30)
                };
                await identityDbContext.RefreshTokens.AddAsync(newToken);
                identityDbContext.RefreshTokens.Remove(oldToken);
                await identityDbContext.SaveChangesAsync();

                var userTokens = new UserTokens
                {
                    AccessToken = tokenGenerator.GenereateAccessToken(oldToken.AppUser),
                    RefreshToken = newToken.Token
                };

                return userTokens;
            }
        }

        /// <summary>
        /// User logout
        /// </summary>
        /// <param name="refresh">Refresh token</param>
        /// <returns>Ok status code</returns>
        [Authorize]
        [HttpPost("logout")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Logout(RefreshCredentials refresh)
        {
            var token = identityDbContext.RefreshTokens.FirstOrDefault(x => x.Token == refresh.RefreshToken);
            if (token != null)
            {
                identityDbContext.RefreshTokens.Remove(token);
                await identityDbContext.SaveChangesAsync();
            }
            return Ok();
        }
    }
}