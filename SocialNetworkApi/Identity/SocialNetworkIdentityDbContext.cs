﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkApi.Identity
{
    public class SocialNetworkIdentityDbContext : IdentityDbContext<AppUser>
    {
        public SocialNetworkIdentityDbContext(DbContextOptions<SocialNetworkIdentityDbContext> options) : base(options)
        {
        }

        public DbSet<RefreshToken> RefreshTokens { get; set; }
    }
}
