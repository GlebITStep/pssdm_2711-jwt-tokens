﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkApi.Identity
{
    public class RefreshToken
    {
        public string Id { get; set; }

        [Required]
        public string Token { get; set; }

        public AppUser AppUser { get; set; }
        public string AppUserId { get; set; }

        [Required]
        public DateTime Expires { get; set; }
    }
}
