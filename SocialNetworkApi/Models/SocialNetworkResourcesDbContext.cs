﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkApi.Models
{
    public class SocialNetworkResourcesDbContext : DbContext
    {
        public SocialNetworkResourcesDbContext(DbContextOptions<SocialNetworkResourcesDbContext> options) : base(options)
        {
        }

        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Post> Posts { get; set; }
    }
}
