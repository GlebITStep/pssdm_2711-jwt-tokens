﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkApi.Models
{
    public class Profile
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string FullName { get; set; }

        public string PhotoUrl { get; set; }

        public DateTime? DateOfBirth { get; set; }

        [Required]
        public string AppUserId { get; set; }
    }
}
