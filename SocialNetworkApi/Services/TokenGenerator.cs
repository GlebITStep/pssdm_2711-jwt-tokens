﻿using Microsoft.Extensions.Options;
using SocialNetworkApi.Identity;
using SocialNetworkApi.Options;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SocialNetworkApi.Services
{
    public class TokenGenerator
    {
        private readonly JwtOptions options;

        public TokenGenerator(IOptions<JwtOptions> options)
        {
            this.options = options.Value;
        }

        public string GenereateAccessToken(AppUser user)
        {
            var issuedAt = DateTime.Now;

            var claims = new[]
            {
                 new Claim("id", user.Id),
                 new Claim("sub", user.UserName)
            };

            var jwt = new JwtSecurityToken(
                issuer: options.Issuer,
                audience: options.Audience,
                claims: claims,
                notBefore: issuedAt,
                expires: issuedAt + options.AccessValidFor,
                signingCredentials: options.SigningCredentials);

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }

        public string GenerateRefreshToken()
        {
            return Guid.NewGuid().ToString();
        }
    }
}
