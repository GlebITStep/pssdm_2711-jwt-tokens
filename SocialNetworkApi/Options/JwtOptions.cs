﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkApi.Options
{
    public class JwtOptions
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public TimeSpan AccessValidFor { get; set; }
        public TimeSpan RefreshValidFor { get; set; }
        public SigningCredentials SigningCredentials { get; set; }
    }
}
